import React, { Component } from 'react';
import Layout from './Components/Layout/Layout';
import BurgerBuilder from './Contanirs/BurgerBuilder/BurgerBuilder';
import CheckOut from './Contanirs/CheckOut/CheckOut';
import Orders from './Contanirs/Orders/Orders';
import { Router, Route } from 'react-router-dom';
import history from './utils/history';

class App extends Component {
  render() {
    return (
      <div>
        <Layout>
          <Router history={history}>
            <Route exact path="/" component={BurgerBuilder}></Route>
            <Route path="/Orders" component={Orders}></Route>
            <Route path="/CheckOut" component={CheckOut}></Route>
          </Router>
        </Layout>
      </div>
    );
  }
}
export default App;
