import React from 'react';
import  './Layout.css';
import Aux from '../../hoc/Aux';
import Toolbar from '../Navigation/Toolbar/Toolbar';
import SideDrawer from '../Navigation/SideDrawer/SideDrawer';

const Layout=(props)=>(
    <Aux>
    <div>
        <Toolbar/>
        <SideDrawer/> 
        </div>
    <main className={'C'}>
        {props.children}
    </main>
    </Aux>
);
export default Layout;