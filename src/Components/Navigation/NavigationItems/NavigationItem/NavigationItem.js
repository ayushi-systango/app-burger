import React from 'react';
import './NavigationItem.css';
import {NavLink, Router} from 'react-router-dom';
import history from '../../../../utils/history';

const NavigationItem = props => (
        <li className={'NavigationItem'}>
          <Router history={history}>
          <NavLink to={props.link} exact={props.exact} activeClassName={'active'}> {props.children}</NavLink>
          </Router>
        </li>
);

export default NavigationItem;