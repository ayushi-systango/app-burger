import React from 'react';

import  './NavigationItems.css';
import NavigationItem from './NavigationItem/NavigationItem';

const NavigationItems = props => (
    <ul className={'NavigationItems'}>
        <NavigationItem exact link="/">Burger Builder</NavigationItem>
        <NavigationItem  link="/Orders">Orders</NavigationItem>

    </ul>
);

export default NavigationItems;