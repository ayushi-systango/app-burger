import React from 'react';
import  './Toolbar.css';
import Logo from '../../Logo/Logo'
import NavigationItems from '../NavigationItems/NavigationItems';
//import DrawerToggle from '../SideDrawer/DrawerToggle/DrawerToggle';

const toolbar = props => (
  <header className={'Toolbar'}>
   {/*  <DrawerToggle clicked={props.drawerToggleClicked} />
    <div className={classes.Logo}>
      <Logo />
    </div>
    <nav className={classes.DesktopOnly}>
      <NavigationItems isAuthenticated={props.isAuth} />
    </nav> */}
    <div>MENU</div>
    
    <div><Logo height="80%" /></div>
    <nav>
        <NavigationItems/>
    </nav>
  </header>
);

export default toolbar;