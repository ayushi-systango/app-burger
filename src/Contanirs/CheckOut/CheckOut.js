import React ,{Component} from 'react';
import CheckOutSummary from '../../Components/Order/CheckOutSummary/CheckOutSummary';
import{Route,Router} from 'react-router-dom';
import history from '../../utils/history';
import ContactData from '../CheckOut/ContactData/ContactData';

class CheckOut extends Component
{
    state={
        ingredients:null,
        price:0
    }
     componentWillMount()
    {
        const query = new URLSearchParams(this.props.location.search);
        const ingredients = {};
        let price=0;
        for (let param of query.entries()) 
        {
            if (param[0] === 'price')
            {
                price = param[1];
            } 
            else 
            {
                ingredients[param[0]] = +param[1];
            }
        }
         this.setState({ingredients: ingredients, totalPrice:price}); 
    } 
    checkoutCancelledHandler=()=>
    {
        this.props.history.goBack();

    }
    checkoutContinueHandler=()=>
    {
        this.props.history.replace('/CheckOut/contact-data');

    }
    render()
    {
        return(
            <div>
                <CheckOutSummary ingredients={this.state.ingredients}
                checkoutCancelled={this.checkoutCancelledHandler}
                checkoutContinue={this.checkoutContinueHandler}/>
                <Router history={history}>

                <Route exact path={this.props.match.path + "/contact-data"} 
                render={(props) => (<ContactData ingredients={this.state.ingredients}
                price={this.state.totalPrice} {...props}/>)}/>

                </Router>
            </div>
        );
    }

}
export default CheckOut;