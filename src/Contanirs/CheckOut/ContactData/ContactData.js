import React,{Component} from 'react';
import Button from '../../../Components/UI/Button/Button';
import './ContactData.css';
import axios from '../../../axios-order';
import Spinner from '../../../Components/UI/Spinner/Spinner';
class ContactData extends Component
{
    state={
        name:'',
        email:'',
        address:{
            street:'',
            postalcode:''
        },
        loading:false
    }
    orderHandler=(event)=>
    {
        event.preventDefault();
        this.setState({loading:true});
         const order ={
             ingredients:this.props.ingredients,
             price:this.props.totalPrice,
             customer:
               {
                 name:"ayushi kadam",
                 address:
                   {
                     street:"teeen puliya",
                     zipcode:"432003",
                     country:"India"
                   },
                   email:"akadam@isystango.com"
               },
               deliveryMethod:"fastest"
         }
         axios.post('/orders.json',order)
         .then(response=>
           {
               this.setState({loading:false});
               this.props.history.push('/');
           })
           .catch(error=>
               {
                   this.setState({loading:false});
               })
    }
    render()
    {
        let form=( 
        <form>
            <input className={'Input'} type='text' name="name" placeholder="your name" />
            <input className={'Input'} type='email' name="email" placeholder="your email"/>
            <input className={'Input'} type='text' name="street" placeholder="street"/>
            <input className={'Input'} type='text' name="postalcode" placeholder="postalcode"/>
            <Button btnType="Success" clicked={this.orderHandler}>ORDER</Button>
        </form>);

        if(this.state.loading)
        {
            form=<Spinner/>;
        }
        return(
            <div className={'ContactData'}>
                <h4>Enter Your Contact Detail</h4>
               {form}
            </div>
        );
    }

} 
export default ContactData;