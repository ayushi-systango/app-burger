import React, {Component} from 'react';
import Order from '../../Components/Order/Order';
import axios from '../../axios-order';
import withErrorHandler from ''
class Orders extends Component
{
    state={
        Orders:[],
        loading:true
    }
    componentDidMount()
    {
        axios.get('/orders.json')
        .then(res => {
            fetchedOrders=[];
            for(let key in res.data)
            {
                fetchedOrders.push({
                    ...res.data[key],
                    id:key
                });
            }
            this.setState({ loading:false, Order:fetchedOrders });

        })
        .catch(err => { 
            this.setState({ loading:true });
        });

    }
    render()
    {
        return(
            <div>
                <Order/>
                <Order/>
            </div>
        );
    }
}
export default Orders;