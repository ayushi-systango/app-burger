import axiox from 'axios'
const instance = axiox.create({
    baseURL:'https://burger-builder-c4f06.firebaseio.com/'
});
export default instance;